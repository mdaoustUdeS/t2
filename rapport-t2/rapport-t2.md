---
fontsize: 12pt
lang: fr
lieu: UNIVERSITÉ DE SHERBROOKE
title: RAPPORT DE STAGE
presented-to: Sylvie Lamarche, conseillère en développement professionnel
author: |
        | Matthieu Daoust, stagiaire
        | CRIM (Centre de recherche informatique de Montréal inc.)
        | Développement logiciel / DevOps
        | T2
date: 12 avril 2021
geometry: margin=1in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{1.5}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \lfoot{Matthieu Daoust}
    - \cfoot{\thepage}
    - \rfoot{Matthieu.Daoust@USherbrooke.ca}
---

# Partie A: Stage en développement logiciel / DevOps

## Mise en contexte

J'ai effectué mon stage dans l'équipe de recherche et de développement du Centre de recherche informatique de Montréal (CRIM). J'ai aussi fait partie du groupe DevOps. Ce groupe a pour mission de rechercher et faire la promotion de l'information à propos des bonnes pratiques en développement logiciel, dans le but d'en faire profiter tous les employés du CRIM.

## Résumé du mandat

Durant les quatre mois où j'ai travaillé au CRIM, j'ai pu travailler sur divers projets où j'ai pu en apprendre sur les outils et pratiques DevOps.

D'une part, j'ai eu à comprendre l'infrastructure d'intégration en continue d'un projet de l'entreprise et à améliorer la plateforme afin de faciliter son utilisation. J'ai aussi participé activement à des travaux d'exploration d'une plateforme d'intégration et de déploiement en continue (CI/CD) pour s'assurer qu'elle répond aux besoins de l'entreprise et j'ai communiqué les résultats durant une présentation où tous les employés étaient invités.

D'autre part, j'ai exploré une plateforme d'orchestration de conteneurs (Kubernetes) qui sera bientôt utilisé dans l'entreprise. Je vais prochainement l'intégrer dans un produit en production.

## Conclusion sur mon expérience de stage

Je suis heureux d'avoir vécu une expérience positive de recherche et de développement dans ce centre de recherche. Cela a confirmé mon intérêt pour en apprendre davantage dans le domaine du développement et des opérations.

# Partie B

## L'environnement

> _Décrivez votre milieu de travail, indiquez vos zones de confort et d’inconfort quant à ces conditions et expliquez pourquoi._

J'ai apprécié la flexibilité de choisir l'endroit et le moment où je souhaitais travailler. En effet, la majorité de mon stage s'est déroulé chez moi, où je me suis fabriqué un bureau. J'ai mis en application les conseils ergonomiques pour organiser mon environnement de travail que j'avais reçu lors du stage précédent, ce qui m'a aidé à éviter des blessures. J'ai aussi choisi de travailler durant les moments de la journée où j'étais le plus productif.

Durant mon stage, je n'ai pas reçu d'ordinateur portable de la part de mon employeur. Il m'a fallu faire preuve de créativité pour trouver une façon d'utiliser les outils de communication. Cet inconfort s'est résolu quelques jours après le début du stage.

J'ai eu la chance de travailler sur plusieurs projets, dont certains où j'ai pu collaborer en équipe. J'ai vraiment apprécié les interactions avec mes collègues de travail, qui m'ont permis d'obtenir d'autres points de vue.

## Les connaissances

> _Que savez-vous maintenant, qui vous aidera pour vos prochains stages ou votre emploi? Pourquoi?_

Durant ce stage, j'ai appris beaucoup de notions qui vont m'être utiles lors de mes prochains stages.

D'une part, j'ai eu à me familiariser avec plusieurs plateformes d'intégration en continue. En effet, en adaptant le code de pipeline vers GitLab, j'ai eu à comprendre le fonctionnement des pipelines de GitHub Actions, de Travis CI et de Bitbucket Pipelines. De plus, en améliorant la plateforme d'intégration en continue, j'ai eu à comprendre le fonctionnement de Jenkins. Cette exploration d'outils me sera utile à l'avenir puisque je serai en mesure de m'adapter facilement à plusieurs environnements d'intégration et de déploiement en continue.

D'autre part, j'ai acquis de l'expérience précieuse avec des outils d'opération en production. En effet, je me suis familiarisé avec la plateforme OpenStack, qui est un ensemble de logiciels qui permettent de déployer des infrastructures infonuagiques. Je me suis aussi familiarisé avec la plateforme d'orchestration de conteneurs Kubernetes. Cette expérience est très importante pour être en mesure d'opérer des applications en production.

## Le professionnalisme

> _Avez-vous fait des apprentissages particuliers en lien avec certaines règles ou normes et quel comportement avez-vous adopté?_

Même dans un milieu de travail où les professionnels sont autonomes dans leur gestion du temps, il arrive parfois d'être submergé de distractions, tel que des réunions, des notifications, des courriels, etc. Ces distractions encouragent le changement de contexte fréquent et peuvent diminuer la productivité et la concentration.

Afin de remédier à ce problème, le groupe dans lequel j'ai travaillé a essayé un projet pilote durant mon stage. En effet, ils ont réservé une journée par semaine appelée _Deep Work_ (travail profond). Cette journée est réservée aux travaux individuels et aucune réunion n'est planifiée cette journée.

J'ai apprécié le fait d'avoir une journée par semaine sans distractions. Cela a eu pour effet de créer plus de moments propices aux travaux qui demandent de la concentration et à diminuer les changements de contextes. Cela a aussi permis de condenser les  réunions sur les autres jours de la semaine, ce qui a diminué les espaces libres entre les réunions qui ne pouvaient pas servir pour faire des tâches qui demandent une grande concentration.

## La connaissance de soi

> _Quels intérêts avez-vous découverts ou confirmés lors de cette expérience?_

J'ai adoré travailler dans un environnement en constante évolution. Cela nous oblige à toujours en apprendre davantage. C'est vraiment agréable d'observer la vitesse à laquelle les technologies de l'information se transforment et de se rendre compte qu'on va toujours avoir quelque chose à apprendre. J'ai aussi apprécié travailler avec des spécialistes qui en connaissent beaucoup dans leur domaine. C'est toujours fascinant et inspirant de voir jusqu'où certaines personnes peuvent se rendre.
